# Ansible Development Container

This is my ansible development container.  There are many like it, but this one is mine.

Using the VSCode "Dev Containers" plugin, this allows me to mount and interact with this container while having verson-control over Python and Ansible, and their related plugins, without interferring with System Python or venv's.


## Image Building

Simple image building occurs with `docker build .` from the repository directory.  `--tag=<tag_name>` can provide a friendly tag name, for example: 

```
docker build . --tag=ansible:latest
```

See `man docker-build` for more details on options.

During build, the `pip_requirements.txt` and `ansible_requirements.yaml` files are read.  Requirements can be added to these files to be baked into the container image, or can be added to containers after the image is built (using `pip install` and `ansible-galaxy`, respectively).

## Container Creation

Containers can be created with `docker run`.  This can occur from any directory.  

- To keep the container persistant and running in the backgorund, `--detach` can be used.  
- A friendly name can be used with `--name=<container_name>` or a dynamic `adjective_noun` name will be assigned automatically.  
- The restart policy can be added with `--restart [no|on-failure|always|unless-stopped] to make the container restart/start at boot.
- The last argument should b ethe image tag name, or the hash of the image.

Example, using "ansible:latest" as the image tag:

```
docker run --detach --name=ansible --restart unless-stopped ansible:latest
```

See `man docker-run` for more details on options.


## Entering container shell

The container shell can be enterred with:

```
docker exec -it <container_name> bash
```

You will be brought directly into the `/ansible` directory.

## Demo "Hello World" playbook.

The sample "hello_world.yaml" is included in `/`.  This can be called for a test of ansible's installation and using environment-vars and command-line variables (`-e`)

The playbook has three tasks: 

- Display "Hello World" message
- Display "Hello " followed by the value of ansible-variable `TEST_ARG`
- Display "Hello " followed by the value of environment variable `TEST_ENV`

The following will display "Hello World!" from all three while inside the containers shell: 

```
TEST_ENV=World\! ansible-playbook /hello_world.yaml -e TEST_ARG=World\!
```

From the host running the container, this can also be run *on* the container using `docker exec`: 

```
docker exec ansible sh -c "TEST_ENV=World\! ansible-playbook /hello_world.yaml -e TEST_ARG=World\!"
```

### Bash Script, run in container

This is also stored in this repo's snippets.

The following bash script will do the following: 

- Map the current directory to /ansible
- Load a local .env file (or the value of `$ENV_FILE`) to the system environment
- Run the specified commands in a fresh container based off `ansible:latest` image.
- Destroy the container once complete.

```
#!/bin/bash

docker run --rm \
    --mount type=bind,source="$(pwd)",target=/ansible \
    --env-file <(cat $(pwd)/${ENV_FILE:=.env} 2>/dev/null || echo '') \
    ansible:latest "$@"
```
