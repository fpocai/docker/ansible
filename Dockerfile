FROM    python:3.10.10-bullseye

ENV     ANSIBLE_FORCE_COLOR=True

COPY    pip_requirements.txt .
COPY    ansible_requirements.yaml .
COPY    hello_world.yaml /    

RUN     python3 -m pip install -r pip_requirements.txt --no-cache ;\
        ansible-galaxy collection install -r ansible_requirements.yaml ;\
        rm /pip_requirements.txt && \
        rm /ansible_requirements.yaml

WORKDIR /ansible

ENTRYPOINT ["tail", "-f", "/dev/null"]
